USE villes;

-- Question 1

SELECT ville_nom, departement_nom FROM villes_france_free AS ville 
INNER JOIN departement ON ville.ville_departement = departement.departement_code 
ORDER BY ville.ville_population_2012 DESC LIMIT 10;

-- Question 2

SELECT departement_nom, departement_code, count(ville_nom) AS nb_communes FROM departement 
INNER JOIN villes_france_free ON ville_departement = departement_code
GROUP BY ville_departement 
ORDER BY nb_communes DESC;

-- Question 3

SELECT departement_nom, ville_departement, SUM(ville_surface) AS surface_dep FROM villes_france_free 
LEFT JOIN departement ON departement_code = ville_departement
GROUP BY ville_departement  
ORDER BY surface_dep DESC
LIMIT 10;

-- Question 4

UPDATE villes_france_free 
SET ville_nom = REPLACE(ville_nom, '-', ' ') 
WHERE ville_nom LIKE 'SAINT-%';

SELECT * FROM departement;
SELECT * FROM villes_france_free;