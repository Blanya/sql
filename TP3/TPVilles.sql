CREATE DATABASE villes;

USE villes;

-- Question 1
SELECT * FROM villes_france_free WHERE ville_departement = '46';

-- Question 2
SELECT * FROM villes_france_free WHERE ville_population_2010 >= 10000; 

-- Question 3
SELECT ville_nom FROM villes_france_free WHERE ville_id > 300 AND ville_id < 400;

-- Question 4
SELECT * FROM villes_france_free WHERE ville_departement > 10 AND ville_departement < 15;

-- Question 5 
SELECT ville_nom, ville_population_2012, ville_surface 
FROM villes_france_free WHERE ville_departement = '59' 
ORDER BY ville_slug;

-- Question 6
SELECT count(*) AS nombre_villes_62 FROM villes_france_free WHERE ville_departement = '62';

-- Question 7
SELECT ville_nom FROM villes_france_free WHERE ville_surface > 10 AND ville_densite_2010 <= 2000 AND ville_departement = '59';

-- Question 8
SELECT count(*) AS nombre_villes_59 FROM villes_france_free WHERE ville_departement = '59' AND ville_surface <= 3;

-- Question 9
SELECT * FROM villes_france_free WHERE ville_nom_soundex = 'N630' ORDER BY ville_nom LIMIT 50;


-- Question 1 TP2
SELECT * FROM villes_france_free ORDER BY ville_population_2012 DESC LIMIT 10;

-- Question 2 TP2
SELECT * FROM villes_france_free ORDER BY ville_surface LIMIT 50;

-- Question 3 TP2
SELECT * FROM villes_france_free WHERE ville_departement LIKE '97%';

-- Question 4 TP2
SELECT count(*) AS villes_debutant_par_Saint FROM villes_france_free WHERE ville_nom LIKE 'SAINT%';

-- Question 5 TP2
SELECT   count(ville_nom) AS nbr_doublon, ville_nom
FROM     villes_france_free GROUP BY ville_nom
HAVING   COUNT(ville_nom) > 1 ORDER BY nbr_doublon DESC;

-- Question 6 TP2
SELECT * FROM villes_france_free WHERE ville_surface > (SELECT AVG(ville_surface) FROM villes_france_free);

-- Question 7 TP2 -- SELON POP 2012
SELECT ville_departement, SUM(ville_population_2012) AS sum 
FROM villes_france_free GROUP BY ville_departement HAVING (SUM(ville_population_2012)) > 2000000; 