-- Question 1
SELECT lastName AS Nom, firstName AS Prenom, jobTitle AS Titre FROM employees
ORDER BY Nom;

-- Question 2
SELECT DISTINCT lastName AS Nom  FROM employees 
ORDER BY Nom;

-- Question 3
SELECT customerName AS Societe, contactLastName AS Nom, contactFirstName AS Prenom FROM customers
WHERE country = 'USA'; 

-- Question 4
SELECT email FROM employees 
WHERE firstName LIKE '%y' AND reportsTo LIKE '105%';


-- Question 5
SELECT lastName AS Nom, email FROM employees 
WHERE firstName LIKE 'L%';

-- Question 6
SELECT * FROM customers
ORDER BY contactLastName;

-- Question 7
SELECT * FROM employees
ORDER BY officeCode;

-- Question 8
SELECT c.customerNumber, c.customerName, SUM(priceEach*quantityOrdered) AS depense FROM customers AS c
INNER JOIN orders ON c.customerNumber = orders.customerNumber 
INNER JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
GROUP BY c.customerNumber
HAVING depense > 100000;

SELECT c.customerNumber, c.customerName AS depense FROM customers AS c
INNER JOIN payments ON c.customerNumber = payments.customerNumber 
WHERE payments.amount > 100000
GROUP BY c.customerNumber;

-- same with natural join 
/* SELECT customerNumber, customerName, SUM(priceEach*quantityOrdered) AS depense FROM customers 
NATURAL JOIN orders
NATURAL JOIN orderdetails
GROUP BY customers.customerNumber
HAVING depense > 100000; */

-- Question 9
SELECT orders.orderNumber, status, SUM(priceEach*quantityOrdered) AS total_ventes FROM orders
INNER JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
GROUP BY orders.orderNumber;

-- Question 10
SELECT orderNumber, productName, msrp, priceEach FROM orderdetails
INNER JOIN products ON orderdetails.productCode = products.productCode
WHERE products.productCode = 'S10_1678' AND msrp > priceEach
GROUP BY orderNumber;

-- Question 11
SELECT email FROM employees
INNER JOIN offices ON employees.officeCode = offices.officeCode
WHERE employees.firstName LIKE '%y' OR offices.city = 'San Francisco';

-- Question 12 
SELECT count(*) FROM customers
INNER JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
WHERE employees.firstName = 'Leslie' AND employees.lastName = 'Jennings';

-- Question 13
SELECT firstName AS Prenom, lastName AS Nom FROM employees
WHERE jobTitle = 'President';

-- Question 14
SELECT SUM(amount) AS somme FROM payments
WHERE paymentDate LIKE '2005-03-%';

-- Question 15
SELECT customerName, SUM(amount) AS total FROM payments
INNER JOIN customers ON customers.customerNumber = payments.customerNumber
GROUP BY customers.customerName;

-- Question 16
SELECT customerNumber, orderDate FROM orders
WHERE status = 'Cancelled';

-- Question 17
SELECT lastName, firstName FROM employees 
WHERE reportsTo = (SELECT employeeNumber FROM employees WHERE firstName = 'Anthony' AND lastName = 'BOW') AND firstName != 'Anthony' AND lastName != 'Bow';

-- Question 18
SELECT e.lastName, e.firstName, count(e.reportsTo) AS refersTo FROM employees AS e 
INNER JOIN employees ON employees.employeeNumber = e.employeeNumber
GROUP BY e.reportsTo
HAVING refersTo = 0;

SELECT firstName, lastName FROM employees 
WHERE reportsTo IS NULL;

-- Question 19
SELECT * FROM orderDetails
WHERE quantityOrdered = (SELECT MIN(quantityOrdered) FROM orderDetails);

-- Question 20
SELECT od.orderNumber, productCode, quantityOrdered, priceEach, orderLineNumber FROM orderdetails AS od
INNER JOIN orders ON orders.orderNumber = od.orderNumber
WHERE orders.orderDate LIKE '2003-04-21%';

-- Question 21
SELECT concat(m.lastName, ' ', m.firstName) AS Manager, concat(e.lastName, ' ', e.firstName) AS Employe FROM employees AS m
INNER JOIN employees AS e ON m.employeeNumber = e.reportsTo
WHERE m.jobTitle LIKE '%manager%';

-- Question 22
SELECT concat(m.lastName, ' ', m.firstName) AS Manager, count(*) AS nb_employe FROM employees AS m
INNER JOIN employees AS e ON m.employeeNumber = e.reportsTo
WHERE m.jobTitle LIKE '%manager%'
GROUP BY m.firstName;