-- Question 1
CREATE DATABASE db1;

USE db1;

-- Question 2
CREATE TABLE employe(
	nom VARCHAR(50),
    prenom VARCHAR(50),
    age TINYINT,
    telephone VARCHAR(12),
    mail VARCHAR(30),
    adresse VARCHAR(255), 
    code_postal VARCHAR(5),
    ville VARCHAR(20)
);

CREATE TABLE clients LIKE employe;
CREATE TABLE manager LIKE employe;

-- Question 3
INSERT INTO clients (nom, prenom, age, telephone, mail, adresse, code_postal, ville) VALUES('Marco','Paul', 45, '0234567865', 'laurent.mike@gmail.com','56 rue de la Lys', '56908', 'Halluin' );

-- Question 4
INSERT INTO employe (nom, prenom, age, telephone, mail, adresse, code_postal, ville) VALUES('Michel','Martin', 67, '0234967865', 'alain.deloinmaisdetresloin@gmail.com', '56 rue des Flandres', '31908', 'Toulouse');
-- Adresse mail trop longue ( effectuée avec un warning, adresse mail coupée)

ALTER TABLE employe MODIFY mail VARCHAR(70);

-- Question 5
INSERT INTO manager (nom, prenom, age, telephone, mail, adresse, code_postal, ville) VALUES('Laurent','Pierre', 34, '0934467805', 'l.pierre@gmail.com', '67 rue des Mers', '31908', 'Toulouse' );
 -- MODIF => Simple quote autour du telephone
 
SELECT * FROM clients;
SELECT * FROM manager;
SELECT * FROM employe;

-- Question 6
ALTER TABLE clients ADD date_inscription DATE;

-- Question 7
INSERT INTO clients(nom, prenom, adresse, code_postal, ville, date_inscription) VALUES('Robert', 'Alain', '45 rue des Champs', '75000', 'Paris', '2021-01-15');

-- Question 8
ALTER TABLE manager ADD date_arrivee DATE;
ALTER TABLE employe ADD date_arrivee DATE;

-- Question 9
ALTER TABLE manager RENAME responsable;
SELECT * FROM responsable;

-- Question 10
ALTER TABLE responsable CHANGE ville city VARCHAR(50);
ALTER TABLE employe CHANGE ville city VARCHAR(50);
ALTER TABLE clients CHANGE ville city VARCHAR(50);

-- Question 11
CREATE TABLE prospect(
	id INT PRIMARY KEY AUTO_INCREMENT,
    nom VARCHAR(20),
    prenom VARCHAR(20),
    age TINYINT(3),
    date_contact DATE,
    telephone VARCHAR(12),
    mail VARCHAR(50),
    adresse VARCHAR(100),
    code_postal VARCHAR(5),
    ville VARCHAR(30),
    revenu_annuel DECIMAL(9,2)
); 

-- Question 12
ALTER TABLE prospect ADD CONSTRAINT c_age CHECK (age>=18), ADD CONSTRAINT cu_nom_prenom UNIQUE(nom, prenom);

-- Question 13
ALTER TABLE prospect MODIFY telephone VARCHAR(50) DEFAULT 'Pas de téléphone';
ALTER TABLE prospect MODIFY mail VARCHAR(50) DEFAULT 'Pas de mail';

-- Question 14
ALTER TABLE prospect MODIFY prenom VARCHAR(50) NOT NULL, MODIFY nom VARCHAR(50) NOT NULL;

SELECT * FROM prospect;

-- Question 15
INSERT INTO prospect(nom, prenom, age, date_contact, telephone, mail, adresse, code_postal, ville, revenu_annuel) 
VALUES('Paul', 'Loco', 45, '2022-05-10', '0525030206', 'myMail@mail.fr', '25 rue des fleurs', '62400', 'Bethune', 44000.89);
INSERT INTO prospect(nom, prenom, age, date_contact, mail, adresse, code_postal, ville, revenu_annuel) 
VALUES ('Michel', 'Boubou', 22, '2022-03-25', 'michbou@mail.fr', '48 rue des lilas', '59000', 'Lille', 32004.895);
INSERT INTO prospect(nom, prenom, age, date_contact, telephone, adresse, code_postal, ville, revenu_annuel)
VALUES ('Louise', 'Michel', 25, '2022-07-01', '0525030206', '48 boulevard de l\'étoile', '62400', 'Bethune', 54980.9);

-- Question 16
INSERT INTO prospect (nom, prenom, age, date_contact, telephone, mail, adresse, code_postal, ville) 
VALUES('Mike','Jim', 18, '2021-09-07' ,'0934467805', 'mike.jim@gmail.com', '67 rue des Mers', '31908', 'Lyon');
-- Modification de l'age 16 en 18 => contrainte majeur + manque revenu !! (retrait de la colonne)

-- Question 17
INSERT INTO prospect (id,nom, prenom, age, date_contact,telephone, mail, adresse, code_postal, ville, revenu_annuel) 
VALUES (23, 'Tom','Jones', 34, '2021-09-07' ,'0734467805', 'tom.jones@gmail.com', '67 rue des Postes', '59908', 'Lille', 45000 );
-- Ajout du revenu annuel, id = 23

-- Question 18
INSERT INTO prospect (nom, prenom, age, date_contact,telephone, mail, adresse, code_postal, ville, revenu_annuel) 
VALUES ('Tina','Hike', 78, '2021-09-14' ,'0634467805', 'mike.jim@gmail.com', '67 rue des Flers', '59908', 'Roubaix', 35000 );
-- Ajout du revenu annuel, id = 24

-- Question 19
ALTER TABLE responsable DROP code_postal;

-- Question 20
DROP TABLE clients, employe;