CREATE DATABASE ex1;

USE ex1;

CREATE TABLE personne(
	id_person INT PRIMARY KEY AUTO_INCREMENT,
    titre ENUM('Mr', 'Mme', 'Mlle'),
    prenom VARCHAR(40),
    nom VARCHAR(40),
    email VARCHAR(60),
    telephone VARCHAR(14)
);

-- Question 1
INSERT INTO personne(titre, prenom, nom, email, telephone) 
VALUES('Mlle', 'Tata', 'Toto', 'tatatoto@money.com', '0902020302'), ('Mr', 'Minet', 'Gros', 'grosminet@money.com', '0612151417'), 
('Mme', 'Jane', 'Doe', 'janedoe@mail.com', '0902020302'), ('Mr', 'Bernard', 'Minet', 'bernardminet@money.com', '0614586824'),
('Mme', 'Foo', 'Bar', 'foo@bar.com', '0605458787');
SELECT * FROM personne;

-- Question 2
SELECT * FROM personne ORDER BY nom;

-- Question 3
SELECT * FROM personne ORDER BY titre DESC;

-- Question 4
SELECT * FROM personne WHERE email = 'foo@bar.com';