CREATE DATABASE ex2;

USE ex2;

CREATE TABLE livre(
	id_livre INT PRIMARY KEY AUTO_INCREMENT,
    titre VARCHAR(200),
    auteur VARCHAR(150),
    editeur VARCHAR(150),
    date_publication DATE,
    isbn_10 VARCHAR(10),
    isbn_13 VARCHAR(14)
);

-- Question 3
(SELECT * FROM livre ORDER BY date_publication ASC LIMIT 10) ORDER BY titre;

-- Question 4
SELECT date_publication, auteur, titre FROM livre ORDER BY date_publication ASC LIMIT 10;

-- Question 5
SELECT * FROM livre WHERE auteur = 'Agatha Christie';

-- Question 6
UPDATE livre SET auteur = 'Agatha Christie' WHERE id_livre = (SELECT id_livre FROM livre WHERE auteur = 'Agatha Christies');

-- Question 7
INSERT INTO livre (titre, auteur, editeur, date_publication, isbn_10, isbn_13) 
VALUES ('Joyland', 'Stephen King', 'Albin Michel', '2013-06-04', '201205845X', '978-201256021');

SELECT * FROM livre;

-- Question 8
-- MYSQL session has the safe_update options, can't delete without id pk => define sql safe to 0
SET SQL_SAFE_UPDATES = 0; 
DELETE FROM livre WHERE auteur = 'Nicolas Caron' OR titre = 'Le Cid'; 